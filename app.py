from api_config import Routes

for r in Routes:
    print('{0} {1} {2}'.format(r.name, r.href, r.operations))
