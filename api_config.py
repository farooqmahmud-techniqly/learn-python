from collections import namedtuple

routes = namedtuple("Routes", "name href operations")

Routes = {
    routes(name="default", href="/api", operations=("GET", "HEAD", "OPTIONS")),
    routes(name="employees", href="/api/employees", operations=("GET", "HEAD", "OPTIONS", "POST")),
    routes(name="employees_by_id", href="/api/employees/{id:int}",
           operations=("GET", "HEAD", "OPTIONS", "POST", "PUT"))}
